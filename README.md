#bitbucket_to_jira

## Description

A repository which can be used during the process of transfering bitbucket issues into Jira issues - by converting the exported bitbucket json file into file suitable for Jira json import.

https://confluence.atlassian.com/adminjiracloud/importing-data-from-bitbucket-935384618.html

By cloning/forking this repository in bitbucket and [setting environment variables](https://confluence.atlassian.com/bitbucket/environment-variables-794502608.html):

* EXTRACT_LOCATION - url of directory containing extracted bitbucket issues
* PROJ_CODE - Project key (to be used in Jira)
* PROJ_DESC - Project description (to be used in Jira)
* PROJ_FILEAME - The prefix for the generated file (so you can find in your project 'Downloads' list)
* PROJ_TITLE - Project title (to be used in Jira)
* BB_AUTH_STRING - user:app password ( [see](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html?_ga=2.76293333.1723136858.1531725104-1557852494.1478786965)  - user must be th eowner of the repository)

the pipelines script can be run to produce a file which can then be [imported into Jira](https://confluence.atlassian.com/adminjiracloud/importing-data-from-json-776636779.html)


## Documentation
	See code

## Languages
	Python 3

## Installation notes
    Tested on Python version 3.5.2, 3.6.6, 3.7.0 .
	
### Running using bitbucket pipelines:
	
* Clone this repository
* As the user who now owns the repository [setup and App Password](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html?_ga=2.76293333.1723136858.1531725104-1557852494.1478786965)
* Export your issues from bitbucket, and extract these into a public location
* Setup the environment variables: 
	* EXTRACT_LOCATION - url of directory containing extracted bitbucket issues
 	* PROJ_CODE - Project key (to be used in Jira)
  	* PROJ_DESC - Project description (to be used in Jira)
  	* PROJ_FILEAME - The prefix for the generated file (so you can find in your project 'Downloads' list)
  	* PROJ_TITLE - Project title (to be used in Jira)
  	* BB_AUTH_STRING - user:app password ( [see](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html?_ga=2.76293333.1723136858.1531725104-1557852494.1478786965)  - user must be th eowner of the repository)
* Run the pipeline

Fingers crossed a file containg your issues, suitable for Jira, can now be found in the **Downloads** section of this repository

## Contributing to the code base
	Clone repository - submit a pull request

## Coding styles
	TODO

## Tests
	TODO

## Issue tracking
	Bitbucket tracker

## Maintenance
	N/A

## History
	Version 0.1

## License
	MIT
