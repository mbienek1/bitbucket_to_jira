import json
import os
import sys
import fileinput
import getopt
import urllib.request
from urllib.error import URLError, HTTPError

comments=[]

def main(argv):
    inputfile = ''
    inputtitle = ''
    inputkey = ''
    inputdescription = ''
    inputlocation = ''
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hf:t:k:d:l:")
    except getopt.GetoptError:
        print("-f <target filename> -t <Title> -k <Key> -d <Description> -l <Location of extract>")
        sys.exit(2)
    if len(opts) != 5:
        print("bad number of options provided: "+str(len(opts))+" -f <target filename> -t <Title> -k <Key> -d <Description> -l <Location of extract>")
        sys.exit(2)

    json_file=""
    for opt, arg in opts:
        if opt == '-h':
            print("-f <target filename> -t <Title> -k <Key> -d <Description> -l <Location of extract>")
            sys.exit()
        elif opt in ("-f"):
            json_file = arg
        elif opt in ("-t"):
            inputtitle = arg
        elif opt in ("-k"):
            inputkey = arg
        elif opt in ("-d"):
            inputdescription = arg
        elif opt in ("-l"):
            inputlocation = arg
            inputfile = inputlocation+"/db-1.0.json"


    inname = inputtitle
    inkey = inputkey
    indescription = inputdescription

    #download from the extract directory (over http) and store in a temporary file
    try:
        local_filename, headers = urllib.request.urlretrieve(inputfile,filename=json_file)
    except HTTPError as e:
        print('The server couldn\'t fulfill the request to '+inputfile)
        print('Error code: ', e.code)
    except URLError as e:
        print('We failed to reach a server '+inputfile)
        print('Reason: ', e.reason)
   
    
    print(inname+'\n',inkey+'\n',indescription+'\n',json_file)

    f = open(json_file, "r")
    filedata = f.read()
    f.close()

    replacetitle = filedata.replace('"title":', '"summary":')

    f = open(json_file, "w")
    f.write(replacetitle)
    f.close()

    f = open(json_file, "r")
    filedata = f.read()
    f.close()

    replacecontent = filedata.replace('"content":', '"description":')

    f = open(json_file, "w")
    f.write(replacecontent)
    f.close()

    f = open(json_file, "r")
    filedata = f.read()
    f.close()

    replacekind = filedata.replace('"kind":', '"issuetype":')

    f = open(json_file, "w")
    f.write(replacekind)
    f.close()

    with open(json_file, "r") as read_file:
        data = json.load(read_file)

    commentsid = {}
    commentsid = data['comments']
    comments_by_issue = {}

    comment_mappings = {"description": "body", "created_on": "created", "user": "author"}
    
    for comment in commentsid:
        #print(comment['issue'])
        if comment['issue'] in comments_by_issue:
            comments_by_issue[comment['issue']].append(comment)
        else:
            comments_by_issue[comment['issue']] = [comment]
    #print(json.dumps(comments_by_issue ,indent=2))

    attachment_mappings = {"filename": "name", "user": "attacher", "path": "uri"}

    #print(json.dumps(data['attachments'],indent=2))

    attachmentsid = {}
    attachmentsid = data['attachments']
    attachments_by_issue = {}

    for attachment in attachmentsid:
        #print(attachment['issue'])
        if attachment['issue'] in attachments_by_issue:
            attachments_by_issue[attachment['issue']].append(attachment)
        else:
            attachments_by_issue[attachment['issue']] = [attachment]
    #print(json.dumps(attachments_by_issue ,indent=2))


    #print(json.dumps(data['issues'],indent=2))
    issuesid = []
    issuesid = data['issues']
    #print(issuesid[4]['id'])
    issues_by_id = {}

    status_mappings = {"new": "backlog", "open": "in progress", "on hold": "selected for development", "resolved": "done", "wontfix": "done", "closed": "done"}

    for issue in issuesid:
        #print(issue['id'])

        if issue['status'] in status_mappings:
            issue['status'] = status_mappings[issue['status']]      
        if issue['id'] in attachments_by_issue:
            issue['attachments'] = attachments_by_issue[issue['id']]
        if issue['id'] in comments_by_issue:
            issue['comments']=[]
            for tmpcomment in comments_by_issue[issue['id']]:
                comment={}
                for key, value in comment_mappings.items():
                    if value == 'body' and tmpcomment[key] is None :
                        tmpcomment[key]=" "
                    comment[value]=tmpcomment[key]
                issue['comments'].append(comment)
        if issue['id'] in attachments_by_issue:
            issue['attachments']=[]
            for tmpattachment in attachments_by_issue[issue['id']]:
                attachment={}
                for key, value in attachment_mappings.items():
                    attachment[value]=tmpattachment[key]
                if inputlocation != '':
                    attachment['uri']=inputlocation + attachment['uri']
                attachment['description']="not set"
                attachment['created']=issue['created_on']
                issue['attachments'].append(attachment)



    projects=[{"name":inname,"key":inkey,"description":indescription,"type": "software","template":"com.pyxis.greenhopper.jira:gh-kanban-template","issues":issuesid}]

    print('"projects":')
    print(json.dumps(projects,indent=2))

    filename = json_file.split('.')

    wr = open(filename[0] + 'convert.json', "w")
    #wr.write('"projects":')
    wr.write('{"projects":'+json.dumps(projects,indent=2)+"}")
    wr.close()


if __name__ == "__main__":
    main(sys.argv[1:])

